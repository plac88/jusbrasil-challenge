package cfg

import "os"

var (
	App = &AppConfig{
		Address: getEnvStr("APP_ADDRESS", ":8080"),
	}
)

type AppConfig struct {
	Address string
}

func getEnvStr(env string, def string) string {
	val := os.Getenv(env)

	if val == "" {
		return def
	}

	return val
}
