FROM golang:latest

WORKDIR /scraper

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build -o scraper .

CMD ["./scraper"]