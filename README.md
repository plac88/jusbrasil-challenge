# Jusbrasil Challenge

### Project

Lawsuit data scraping from MS and AL courts, first and second instance.

- `/cmd`: command line tool to ease data visualization
- `colly`: go framework for crawling and scraping

### Running

Tests:
> go test ./...

API:
> docker-compose up

Scraping a lawsuit:
```
curl -X POST \
     -H "Content-Type: application/json" \
     -d '{"lawsuit_number": "0000149-67.2014.8.02.0068"}' \
     localhost:8000/lawsuit
```

> :coffee:
