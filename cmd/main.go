package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/plac/challenge/scrapers"
)

func print(content scrapers.Lawsuit) {
	j, err := json.MarshalIndent(content, "", "\t")

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(j))
}

func main() {

	parser := scrapers.MustNewParser()

	scraperFirst := scrapers.MustNewScraper(map[string]scrapers.Scraper{
		"8.02": scrapers.MustNewAlagoasFirstIntanceLawsuitScrapeEngine(parser),
		"8.12": scrapers.MustNewMatoGrossoSulFirstIntanceLawsuitScrapeEngine(parser),
	})

	first, err := scraperFirst.ScrapeLawsuit(os.Args[1])

	if err != nil {
		panic(err)
	}

	print(first)

	scraperSecond := scrapers.MustNewScraper(map[string]scrapers.Scraper{
		"8.02": scrapers.MustNewAlagoasSecondIntanceLawsuitScrapeEngine(parser),
		"8.12": scrapers.MustNewMatoGrossoSulSecondIntanceLawsuitScrapeEngine(parser),
	})

	second, err := scraperSecond.ScrapeLawsuit(os.Args[1])

	if err != nil {
		panic(err)
	}

	print(second)
}
