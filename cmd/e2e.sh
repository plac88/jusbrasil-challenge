#!/bin/bash

go build -o scraper cmd/main.go

echo ""
echo "8.02"
echo ""

./scraper 0000149-67.2014.8.02.0068 > sample.txt
./scraper 0710802-55.2018.8.02.0001 >> sample.txt

echo ""
echo "8.12"
echo ""

./scraper 0801273-22.2015.8.12.0009 >> sample.txt
./scraper 0821901-51.2018.8.12.0001 >> sample.txt

gedit sample.txt &
rm scraper
