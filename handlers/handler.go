package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/plac/challenge/scrapers"
)

type Handler struct {
	scrapers []scrapers.Scraper
}

func NewHandler(scrapers ...scrapers.Scraper) *Handler {
	return &Handler{
		scrapers: scrapers,
	}
}

func (h *Handler) Listen(port string) {
	http.Handle("/", http.HandlerFunc(h.handleRoot))
	http.Handle("/lawsuit", http.HandlerFunc(h.handleScrapeLawsuit))

	fmt.Printf("Listening on %s\n", port)
	http.ListenAndServe(port, nil)
}

func (h *Handler) handleRoot(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (h *Handler) handleScrapeLawsuit(w http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	decoder := json.NewDecoder(req.Body)
	var p ScrapePayload
	err := decoder.Decode(&p)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	var response ScrapeResponse

	for _, scraper := range h.scrapers {
		l, err := scraper.ScrapeLawsuit(p.LawsuitNumber)

		if err != nil {
			response.Errors = append(response.Errors, err.Error())
		} else {
			if !l.Empty() {
				response.Lawsuits = append(response.Lawsuits, l)
			}
		}
	}

	json.NewEncoder(w).Encode(response)
}
