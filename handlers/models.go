package handlers

import "github.com/plac/challenge/scrapers"

type ScrapePayload struct {
	LawsuitNumber string `json:"lawsuit_number"`
}

type ScrapeResponse struct {
	Lawsuits []scrapers.Lawsuit `json:"lawsuits"`
	Errors   []string           `json:"errors"`
}
