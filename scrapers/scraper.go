package scrapers

import (
	"fmt"
	"regexp"
)

func NewInvalidLawsuitNumberError(lawsuitNumber string) error {
	return fmt.Errorf("invalid lawsuit number '%s'", lawsuitNumber)
}

func NewInexistentScraperForLawsuitNumber(lawsuitNumber string) error {
	return fmt.Errorf("no scraper found for court code found in '%s'", lawsuitNumber)
}

type Scraper interface {
	ScrapeLawsuit(lawsuitNumber string) (Lawsuit, error)
}

type scraper struct {
	scrapers map[string]Scraper
}

func validate(lawsuitNumber string) error {
	matched, err := regexp.Match(`\d{7}-\d\d\.\d{4}\.\d.\d\d\.\d{4}`, []byte(lawsuitNumber))
	if err != nil {
		return err
	}

	if !matched {
		return NewInvalidLawsuitNumberError(lawsuitNumber)
	}

	return nil
}

func (s *scraper) ScrapeLawsuit(lawsuitNumber string) (lawsuit Lawsuit, err error) {

	if err = validate(lawsuitNumber); err != nil {
		return
	}

	courtCode := lawsuitNumber[16:20]

	scraper, ok := s.scrapers[courtCode]

	if !ok {
		err = NewInexistentScraperForLawsuitNumber(lawsuitNumber)
		return
	}

	return scraper.ScrapeLawsuit(lawsuitNumber)
}

func MustNewScraper(scrapers map[string]Scraper) Scraper {
	return &scraper{
		scrapers: scrapers,
	}
}
