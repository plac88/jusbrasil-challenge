package scrapers

import (
	"fmt"
	"regexp"
	"strings"
)

type Parser interface {
	ParseLawsuitData(raw string) (Lawsuit, error)
	ParseMovements(raw string) ([]LawsuitMovement, error)
	ParseParts(raw string) ([]LawsuitPart, error)
}

type parser struct{}

func MustNewParser() Parser {
	return &parser{}
}

func sanitize(a string) string {
	a = strings.ReplaceAll(a, "\\t", "")
	a = strings.ReplaceAll(a, "\\n", " ")
	a = strings.ReplaceAll(a, "\\u00a0", "")
	a = strings.ReplaceAll(a, "\\", "")
	a = strings.ReplaceAll(a, " :", ":")
	a = strings.Join(strings.Fields(a), " ")
	strings.Join(strings.Fields(a), " ")
	return a
}

var (
	LawsuitDataRegexp      = `(?m)\B[a-zA-Z\sWíÁãçáé]+:`
	MovementLeadDateRegexp = `(?m)^\d\d\/\d\d\/\d{4}|\B\d\d\/\d\d\/\d{4}`
	PartRoleNameRegexp     = `(?m)\B[\w Wíé]*:`
)

func escapeString(s string) string {
	return fmt.Sprintf("%q", s)
}

var (
	ClassIdentifier             = "Classe:"
	AreaIdentifier              = "Área:"
	SubjectIdentifier           = "Assunto:"
	DistribuitionDateIdentifier = "Distribuição:"
	JudgeIdentifier             = "Juiz:"
	ActValueIdentifier          = "Valor da ação:"
)

func toDict(raw string, idxs [][]int) map[string]string {
	dict := make(map[string]string)
	cutUntil := len(raw) - 1
	for i := len(idxs) - 1; i >= 0; i-- {
		key := raw[idxs[i][0]:idxs[i][1]]
		value := raw[idxs[i][1]:cutUntil]
		key = strings.TrimSpace(sanitize(key))
		value = strings.TrimSpace(sanitize(value))

		dict[key] = value

		cutUntil = idxs[i][0]
	}

	return dict
}

func (p *parser) ParseLawsuitData(raw string) (Lawsuit, error) {
	raw = escapeString(raw)
	e := regexp.MustCompile(LawsuitDataRegexp)
	idxs := e.FindAllStringIndex(raw, -1)
	dict := toDict(raw, idxs)

	var lawsuit Lawsuit

	if v, ok := dict[ClassIdentifier]; ok {
		lawsuit.Class = v
	}
	if v, ok := dict[AreaIdentifier]; ok {
		lawsuit.Area = v
	}
	if v, ok := dict[SubjectIdentifier]; ok {
		lawsuit.Subject = v
	}
	if v, ok := dict[DistribuitionDateIdentifier]; ok {
		lawsuit.DistributionDate = v
	}
	if v, ok := dict[JudgeIdentifier]; ok {
		lawsuit.Judge = v
	}
	if v, ok := dict[ActValueIdentifier]; ok {
		lawsuit.ActValue = v
	}
	return lawsuit, nil
}

func (p *parser) ParseMovements(raw string) ([]LawsuitMovement, error) {
	raw = escapeString(raw)
	e := regexp.MustCompile(MovementLeadDateRegexp)
	idxs := e.FindAllStringIndex(raw, -1)
	movements := make([]LawsuitMovement, len(idxs))
	cutUntil := len(raw) - 1
	for i := len(idxs) - 1; i >= 0; i-- {
		date := raw[idxs[i][0]:idxs[i][1]]
		movement := raw[idxs[i][1]:cutUntil]

		movements[i] = LawsuitMovement{
			Date:     date,
			Movement: sanitize(movement),
		}

		cutUntil = idxs[i][0]
	}

	return movements, nil
}

func (p *parser) ParseParts(raw string) ([]LawsuitPart, error) {
	var e = regexp.MustCompile(PartRoleNameRegexp)
	raw = escapeString(raw)
	idxs := e.FindAllStringIndex(raw, -1)
	parts := make([]LawsuitPart, len(idxs))
	cutUntil := len(raw) - 1
	for i := len(idxs) - 1; i >= 0; i-- {
		role := raw[idxs[i][0] : idxs[i][1]-1]
		name := raw[idxs[i][1]:cutUntil]

		parts[i] = LawsuitPart{
			Role: sanitize(role),
			Name: sanitize(name),
		}

		cutUntil = idxs[i][0]
	}

	return parts, nil
}
