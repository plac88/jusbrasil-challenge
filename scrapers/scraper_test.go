package scrapers

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type scraperMock struct {
	mock.Mock
}

func (s *scraperMock) ScrapeLawsuit(lawsuitNumber string) (Lawsuit, error) {
	args := s.Called(lawsuitNumber)
	return Lawsuit{}, args.Error(1)
}

func TestInvalidLawsuitNumber(t *testing.T) {
	numbers := []string{"12387,.23.2.3.22", "", "aef89fh494f4f44f4f"}

	for _, lawsuitNumber := range numbers {
		scraper := MustNewScraper(map[string]Scraper{})

		_, err := scraper.ScrapeLawsuit(lawsuitNumber)

		assert.Equal(t, err, NewInvalidLawsuitNumberError(lawsuitNumber))
	}
}

func TestLawsuitNumberScrapeForCourtCodeInexistent(t *testing.T) {
	lawsuitNumber := "0710802-55.2018.8.02.0001"
	scraper := MustNewScraper(map[string]Scraper{})

	_, err := scraper.ScrapeLawsuit(lawsuitNumber)

	assert.Equal(t, err, NewInexistentScraperForLawsuitNumber(lawsuitNumber))
}

func TestShouldUseScraperBasedOnCourtCode(t *testing.T) {
	lawsuitNumber := "0710802-55.2018.8.02.0001"
	first := new(scraperMock)
	second := new(scraperMock)
	first.On("ScrapeLawsuit", lawsuitNumber).Return(Lawsuit{}, nil)
	scrapers := map[string]Scraper{"8.02": first, "8.19": second}

	scraper := MustNewScraper(scrapers)

	_, err := scraper.ScrapeLawsuit(lawsuitNumber)

	assert.NoError(t, err)
	first.AssertExpectations(t)
	second.AssertExpectations(t)
}
