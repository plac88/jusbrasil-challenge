package scrapers

import (
	"github.com/gocolly/colly"
)

const (
	TJMS_first_instance_url_template ScraperUrlTemplate = "https://esaj.tjms.jus.br/cpopg5/search.do?conversationId=&dadosConsulta.localPesquisa.cdLocal=-1&cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado={YEAR_DIGIT_UNIFIED}&foroNumeroUnificado={VENUE}&dadosConsulta.valorConsultaNuUnificado={UNIFIED}&dadosConsulta.valorConsulta=&uuidCaptcha="
)

type scrapeEngineFirstMS struct {
	scrapeEngine

	collector *colly.Collector
	parser    Parser
}

func MustNewMatoGrossoSulFirstIntanceLawsuitScrapeEngine(parser Parser) Scraper {
	return &scrapeEngineFirstMS{
		collector: colly.NewCollector(),
		parser:    parser,
		scrapeEngine: scrapeEngine{
			string(TJMS_first_instance_url_template),
		},
	}
}

func (s *scrapeEngineFirstMS) ScrapeLawsuit(lawsuitNumber string) (lawsuit Lawsuit, err error) {

	c := colly.NewCollector()

	lawsuit.Instance = First

	c.OnHTML("body > div > table:nth-child(4) > tbody > tr > td > div:nth-child(7) > table.secaoFormBody", func(e *colly.HTMLElement) {
		data, _ := s.parser.ParseLawsuitData(e.Text)

		lawsuit.Class = data.Class
		lawsuit.Area = data.Area
		lawsuit.Subject = data.Subject
		lawsuit.DistributionDate = data.DistributionDate
		lawsuit.Judge = data.Judge
		lawsuit.ActValue = data.ActValue
	})

	c.OnHTML("#tablePartesPrincipais", func(e *colly.HTMLElement) {
		parts, _ := s.parser.ParseParts(e.Text)
		lawsuit.Parts = parts
	})

	c.OnHTML("#tabelaUltimasMovimentacoes", func(e *colly.HTMLElement) {
		movements, _ := s.parser.ParseMovements(e.Text)
		lawsuit.Movements = movements
	})

	url := s.generateURL(lawsuitNumber)
	err = c.Visit(url)

	return
}
