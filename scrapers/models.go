package scrapers

type Instance int8

const (
	First Instance = iota + 1
	Second
)

type Lawsuit struct {
	Instance         Instance
	Class            string
	Area             string
	Subject          string
	DistributionDate string
	Judge            string
	ActValue         string
	Parts            []LawsuitPart
	Movements        []LawsuitMovement
}

func (l Lawsuit) Empty() bool {
	return len(l.Class) == 0 && len(l.Area) == 0 && len(l.Subject) == 0 && len(l.Parts) == 0 && len(l.Movements) == 0
}

type LawsuitMovement struct {
	Date     string
	Movement string
}

type LawsuitPart struct {
	Role string
	Name string
}
