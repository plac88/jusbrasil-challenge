package scrapers

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestURLTemplatesShouldHaveReplaceFields(t *testing.T) {
	urls := []string{
		string(TJAL_first_instance_url_template),
		string(TJAL_second_instance_url_template),
		string(TJMS_first_instance_url_template),
		string(TJMS_second_instance_url_template),
	}

	for _, url := range urls {
		assert.True(t, strings.Contains(url, YearDigitUnified))
		assert.True(t, strings.Contains(url, Venue))
		assert.True(t, strings.Contains(url, Unified))
	}
}
func TestGenerateURL(t *testing.T) {
	base := scrapeEngine{
		url: string(TJAL_first_instance_url_template),
	}

	r := base.generateURL("0000149-67.2014.8.02.0068")

	assert.Equal(t, r, "https://www2.tjal.jus.br/cpopg/search.do?conversationId=&dadosConsulta.localPesquisa.cdLocal=-1&cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado=0000149-67.2014&foroNumeroUnificado=0068&dadosConsulta.valorConsultaNuUnificado=0000149-67.2014.8.02.0068&dadosConsulta.valorConsulta=&uuidCaptcha=")
}
