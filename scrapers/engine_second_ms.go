package scrapers

import (
	"fmt"

	"github.com/gocolly/colly"
)

const (
	TJMS_second_instance_url_template ScraperUrlTemplate = "https://esaj.tjms.jus.br/cposg5/search.do?conversationId=&paginaConsulta=1&localPesquisa.cdLocal=-1&cbPesquisa=NUMPROC&tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado={YEAR_DIGIT_UNIFIED}&foroNumeroUnificado={VENUE}&dePesquisaNuUnificado={UNIFIED}&dePesquisa=&uuidCaptcha="
)

type scrapeEngineSecondMS struct {
	scrapeEngine

	collector *colly.Collector
	parser    Parser
}

func MustNewMatoGrossoSulSecondIntanceLawsuitScrapeEngine(parser Parser) Scraper {
	return &scrapeEngineSecondMS{
		collector: colly.NewCollector(),
		parser:    parser,
		scrapeEngine: scrapeEngine{
			string(TJMS_second_instance_url_template),
		},
	}
}

func (s *scrapeEngineSecondMS) ScrapeLawsuit(lawsuitNumber string) (lawsuit Lawsuit, err error) {

	lawsuit.Instance = Second

	s.collector.OnHTML("body > div > table:nth-child(4) > tbody > tr > td > div:nth-child(7) > table.secaoFormBody", func(e *colly.HTMLElement) {
		data, _ := s.parser.ParseLawsuitData(e.Text)

		lawsuit.Class = data.Class
		lawsuit.Area = data.Area
		lawsuit.Subject = data.Subject
		lawsuit.DistributionDate = data.DistributionDate
		lawsuit.Judge = data.Judge
		lawsuit.ActValue = data.ActValue
	})

	s.collector.OnHTML("#tablePartesPrincipais", func(e *colly.HTMLElement) {
		parts, _ := s.parser.ParseParts(e.Text)
		lawsuit.Parts = parts
	})

	s.collector.OnHTML("#tabelaUltimasMovimentacoes", func(e *colly.HTMLElement) {
		movements, _ := s.parser.ParseMovements(e.Text)
		lawsuit.Movements = movements
	})

	url := s.generateURL(lawsuitNumber)
	fmt.Println("visiting ", url)
	err = s.collector.Visit(url)

	return
}
