package scrapers

import "strings"

type ScraperUrlTemplate string

type scrapeEngine struct {
	url string
}

const (
	YearDigitUnified = "{YEAR_DIGIT_UNIFIED}"
	Unified          = "{UNIFIED}"
	Venue            = "{VENUE}"
)

func (s *scrapeEngine) generateURL(lawsuitNumber string) string {
	url := strings.ReplaceAll(s.url, YearDigitUnified, lawsuitNumber[0:15])
	url = strings.ReplaceAll(url, Unified, lawsuitNumber)
	return strings.ReplaceAll(url, Venue, lawsuitNumber[21:25])
}
