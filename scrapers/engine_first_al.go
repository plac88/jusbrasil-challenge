package scrapers

import (
	"fmt"

	"github.com/gocolly/colly"
)

const (
	TJAL_first_instance_url_template ScraperUrlTemplate = "https://www2.tjal.jus.br/cpopg/search.do?conversationId=&dadosConsulta.localPesquisa.cdLocal=-1&cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado={YEAR_DIGIT_UNIFIED}&foroNumeroUnificado={VENUE}&dadosConsulta.valorConsultaNuUnificado={UNIFIED}&dadosConsulta.valorConsulta=&uuidCaptcha="
)

type scrapeEngineFirstAlagoas struct {
	scrapeEngine

	collector *colly.Collector
	parser    Parser
}

func MustNewAlagoasFirstIntanceLawsuitScrapeEngine(parser Parser) Scraper {
	return &scrapeEngineFirstAlagoas{
		collector: colly.NewCollector(),
		parser:    parser,
		scrapeEngine: scrapeEngine{
			string(TJAL_first_instance_url_template),
		},
	}
}

func (s *scrapeEngineFirstAlagoas) ScrapeLawsuit(lawsuitNumber string) (lawsuit Lawsuit, err error) {

	lawsuit.Instance = First

	s.collector.OnHTML("body > div > table:nth-child(4) > tbody > tr > td > div:nth-child(7) > table.secaoFormBody", func(e *colly.HTMLElement) {
		data, _ := s.parser.ParseLawsuitData(e.Text)

		lawsuit.Class = data.Class
		lawsuit.Area = data.Area
		lawsuit.Subject = data.Subject
		lawsuit.DistributionDate = data.DistributionDate
		lawsuit.Judge = data.Judge
		lawsuit.ActValue = data.ActValue
	})

	s.collector.OnHTML("#tablePartesPrincipais", func(e *colly.HTMLElement) {
		parts, _ := s.parser.ParseParts(e.Text)
		lawsuit.Parts = parts
	})

	s.collector.OnHTML("#tabelaUltimasMovimentacoes", func(e *colly.HTMLElement) {
		movements, _ := s.parser.ParseMovements(e.Text)
		lawsuit.Movements = movements
	})

	url := s.generateURL(lawsuitNumber)
	fmt.Println("visiting ", url)
	err = s.collector.Visit(url)

	return
}
