package main

import (
	"github.com/plac/challenge/cfg"
	"github.com/plac/challenge/handlers"
	"github.com/plac/challenge/scrapers"
)

func main() {

	parser := scrapers.MustNewParser()

	firstScraper := scrapers.MustNewScraper(map[string]scrapers.Scraper{
		"8.02": scrapers.MustNewAlagoasFirstIntanceLawsuitScrapeEngine(parser),
		"8.12": scrapers.MustNewMatoGrossoSulFirstIntanceLawsuitScrapeEngine(parser),
	})

	secondScraper := scrapers.MustNewScraper(map[string]scrapers.Scraper{
		"8.02": scrapers.MustNewAlagoasSecondIntanceLawsuitScrapeEngine(parser),
		"8.12": scrapers.MustNewMatoGrossoSulSecondIntanceLawsuitScrapeEngine(parser),
	})

	handlers.NewHandler(firstScraper, secondScraper).Listen(cfg.App.Address)
}
